package br.com.compasso.service;

import java.math.BigDecimal;
import java.util.List;

import br.com.compasso.model.entity.Product;

public interface ProductService {
	
	Product save(Product product);
	
	Product update(Product product);

	Product getProductById(Long id);

	List<Product> list();

	void delete(Long id);

	List<Product> search(String superget, BigDecimal minPrice, BigDecimal maxPrice);

}
