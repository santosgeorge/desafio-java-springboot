package br.com.compasso.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.compasso.exception.type.NotFoundException;
import br.com.compasso.model.entity.Product;
import br.com.compasso.repository.ProductRepository;
import br.com.compasso.utils.ResourceUtils;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	public Product save(Product product) {
		product.setId(null);
		return this.productRepository.save(product);
	}

	@Override
	public Product update(Product product) {		
		checkExistence(product.getId());
		return this.productRepository.save(product);

	}
	
	@Override
	public Product getProductById(Long id) {
		Product product = checkExistence(id);	
		return product;
	}
	
	@Override
	public List<Product> list() {
		return this.productRepository.findAll();
	}
	
	@Override
	public void delete(Long id) {
		checkExistence(id);
		this.productRepository.deleteById(id);
	}
	
	@Override
	public List<Product> search(String superget, BigDecimal minPrice, BigDecimal maxPrice) {
		superget = superget.equals("null") ? null : superget;
		minPrice = minPrice.compareTo(new BigDecimal(-1)) == 0 ? null : minPrice;
		maxPrice = maxPrice.compareTo(new BigDecimal(-1)) == 0 ? null : maxPrice;

		return this.productRepository.search(superget, minPrice, maxPrice);
	}
	
	private Product checkExistence(Long id) {
		Product productFound = this.productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(ResourceUtils.getMessage("PRODUCT_NOT_FOUND", id)));
		return productFound;
	}

}
