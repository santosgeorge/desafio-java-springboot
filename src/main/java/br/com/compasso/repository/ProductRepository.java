package br.com.compasso.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.compasso.model.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
		    
    @Query("SELECT p FROM Product p "
    		  + " WHERE (:superget is null or p.name LIKE %:superget% or p.description LIKE %:superget%) "
    		  + " AND (:minPrice is null or p.price >= :minPrice) "
    		  + " AND (:maxPrice is null or p.price <= :maxPrice) ")
    List<Product> search(@Param("superget") String superget, 
    		@Param("minPrice") BigDecimal minPrice , 
    		@Param("maxPrice") BigDecimal maxPrice);

}
