package br.com.compasso.exception.type;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7984053193315568229L;
	
	public NotFoundException(String mensagem) {
		super(mensagem);
	}
	
	public NotFoundException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}

}
