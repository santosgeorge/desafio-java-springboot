package br.com.compasso.exception.handler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.compasso.exception.type.NotFoundException;
import br.com.compasso.model.error.ResponseError;
import br.com.compasso.model.error.ResponseError.ObjectError;
import br.com.compasso.utils.ResourceUtils;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<ObjectError> errors = getErrors(ex);
        ResponseError errorResponse = getErrorResponse(ex, status, errors);
        return new ResponseEntity<>(errorResponse, status);
    }
    
	private ResponseError getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status, List<ObjectError> errors) {
		return new ResponseError(status.value(), ResourceUtils.getMessage("DATA_VALIDATION"), errors);
	}

    private List<ObjectError> getErrors(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getFieldErrors().stream()
                .map(error -> new ObjectError(error.getField(), getMessage(error)))
                .collect(Collectors.toList());
    }
    
    private String getMessage(MessageSourceResolvable messageSourceResolvable) {
    	return messageSource.getMessage(messageSourceResolvable, LocaleContextHolder.getLocale());
    }
    
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundExceptionException(NotFoundException ex, WebRequest request) {
        ResponseError error = new ResponseError(HttpStatus.NOT_FOUND.value(), ex.getMessage(), null);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }
    
    @ExceptionHandler(Exception.class)  
    public final ResponseEntity<Object> handleGenericException(Exception ex, WebRequest request) {   
    	ResponseError error = new ResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), null);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    } 

}
