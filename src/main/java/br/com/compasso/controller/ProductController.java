package br.com.compasso.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.compasso.model.entity.Product;
import br.com.compasso.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	/*
	 * CRIAÇÃO DE UM PRODUTO
	 */
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Product save(@RequestBody @Valid Product product) {
		return this.productService.save(product);
	}
	
	/*
	 * ATUALIZAÇÃO DE UM PRODUTO
	 */
	@PutMapping("/{id}")
	public Product update(@PathVariable Long id, @RequestBody @Valid Product product) {
		product.setId(id);
		return this.productService.update(product);
	}
	
	/*
	 * BUSCA DE UM PRODUTO POR ID
	 */
	@GetMapping("/{id}")
	public Product getProductById(@PathVariable Long id) {
		return this.productService.getProductById(id);
	}
	
	/*
	 * LISTA DE PRODUTOS
	 */
	@GetMapping
	public List<Product> list() {
		return this.productService.list();
	}
	
	/*
	 * DELEÇÃO DE UM PRODUTO
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long id) {
		this.productService.delete(id);
	}
	
	/*
	 * LISTA DE PRODUTOS FILTRADOS
	 */
	@GetMapping("/search")
	public List<Product> search(@RequestParam(value = "q", defaultValue = "null") String superget, 
			@RequestParam(value = "min_price", defaultValue = "-1") BigDecimal minPrice, 
			@RequestParam(value = "max_price", defaultValue = "-1") BigDecimal maxPrice) {
		return this.productService.search(superget, minPrice, maxPrice);
		
	}

}
