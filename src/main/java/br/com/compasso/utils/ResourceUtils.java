package br.com.compasso.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.springframework.util.StreamUtils;

public class ResourceUtils {
	
	private static ResourceBundle bundle;
	
    static {
        bundle = ResourceBundle.getBundle("messages_pt_br", new Locale("pt_br"));
    }
    
    public static String getMessage(String key) {
        return bundle.getString(key);
    }
    
    public static String getMessage(String key, Object... params) {
        try {
            return MessageFormat.format(bundle.getString(key), params);
        } catch (MissingResourceException e) {
        	throw new RuntimeException(e);
        }
    }
    
	public static String getContentFromResource(String resourceName) {
		try {
			InputStream stream = ResourceUtils.class.getResourceAsStream(resourceName);
			return StreamUtils.copyToString(stream, Charset.forName("UTF-8"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}	
}
